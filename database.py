import sqlite3


class database_controller:
    def __init__(self):
        self.db = "Histories.db"
        self.conn = sqlite3.connect(self.db)
        self.curser = self.conn.cursor()
        try:
            self.curser.execute('''CREATE TABLE recipts
                                                         (ID INTEGER PRIMARY KEY,
                                                          name text ,
                                                          mobile text,
                                                          hometel text,
                                                          address text,
                                                          serial text,
                                                          ddate  text,
                                                          ttime  text,
                                                          P1stype text,
                                                          P1model  text,
                                                          P1serial text,
                                                          P1cost text,
                                                          P1peigiri text,
                                                          P1irad text,
                                                          P1describtion text,
                                                          P2stype text,
                                                          P2model  text,
                                                          P2serial text,
                                                          P2cost text,
                                                          P2peigiri text,
                                                          P2irad text,
                                                          P2describtion text,
                                                          P3stype text,
                                                          P3model  text,
                                                          P3serial text,
                                                          P3cost text,
                                                          P3peigiri text,
                                                          P3irad text,
                                                          P3describtion text,
                                                          P4stype text,
                                                          P4model  text,
                                                          P4serial text,
                                                          P4cost text,
                                                          P4peigiri text,
                                                          P4irad text,
                                                          P4describtion text)''')
            self.conn.commit()
        except:
            pass

    def add_item(self, moshtari, items):
        serial = moshtari["serial"]
        date = moshtari["date"]
        time = moshtari["time"]
        mobile = moshtari["mobile"]
        hometel = moshtari["hometel"]
        name = moshtari["name"]
        address = moshtari["address"]

        P = [None] * 4
        P[0] = [None] * 7
        P[1] = [None] * 7
        P[2] = [None] * 7
        P[3] = [None] * 7

        for ser in range(len(items)):
            P[ser] = items[ser]

        for pp in P:
            if pp[4]:
                pp[4] = pp[4][0] + "-" + pp[4][1] + "-" + pp[4][2]

        self.curser.execute('SELECT * FROM recipts WHERE serial = (?)', (serial,))
        isit = self.curser.fetchall()
        if not isit:
            self.curser.execute('''INSERT INTO recipts (name, mobile, hometel, address, serial, ddate, ttime,
                                    P1stype, P1model, P1serial, P1cost, P1peigiri, P1irad, P1describtion,
                                    P2stype, P2model, P2serial, P2cost, P2peigiri, P2irad, P2describtion,
                                    P3stype, P3model, P3serial, P3cost, P3peigiri, P3irad, P3describtion,
                                    P4stype, P4model, P4serial, P4cost, P4peigiri, P4irad, P4describtion) VALUES 
                                    (?,?,?,?,?,?,?,
                                     ?,?,?,?,?,?,?,
                                     ?,?,?,?,?,?,?,
                                     ?,?,?,?,?,?,?,
                                     ?,?,?,?,?,?,?)''',
                                (name, mobile, hometel, address, serial, date, time,
                                 P[0][0], P[0][1], P[0][2], P[0][3], P[0][4], P[0][5], P[0][6],
                                 P[1][0], P[1][1], P[1][2], P[1][3], P[1][4], P[1][5], P[1][6],
                                 P[2][0], P[2][1], P[2][2], P[2][3], P[2][4], P[2][5], P[2][6],
                                 P[3][0], P[3][1], P[3][2], P[3][3], P[3][4], P[3][5], P[3][6]
                                 ))

        self.conn.commit()

    def search(self,ser,name,model,mobile):
        if ser:
            self.curser.execute('''select * from recipts where serial = ?''',(ser,))
            res = self.curser.fetchall()
            if res:
                return res[0]
            else:
                return "wrong"
        elif name:
            self.curser.execute('''select * from recipts where name = ?''', (name,))
            res = self.curser.fetchall()
            if res:
                return res[0]
            else:
                return "wrong"
        elif model:
            self.curser.execute('''select * from recipts where P1model = ? or P2model= ? or P3model=? or P4model=?''',
                                (model,model,model,model,))
            res = self.curser.fetchall()
            if res:
                return res[0]
            else:
                return "wrong"
            pass
        else:
            self.curser.execute('''select * from recipts where mobile = ?''', (mobile,))
            res = self.curser.fetchall()
            if res:
                return res[0]
            else:
                return "wrong"
            pass
