from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.popup import Popup
from kivy.core.window import Window
from kivy.uix.behaviors.focus import FocusBehavior
from bidi.algorithm import get_display
import arabic_reshaper
from kivy.clock import Clock
from persiantools.jdatetime import JalaliDateTime
from kivy.properties import StringProperty, NumericProperty, ObjectProperty
import printer as pr
import database

Window.maximize()


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


class Label_f(Label):
    def __init__(self, *args, **kwargs):
        super(Label_f, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)


class Text_f(TextInput):
    max_chars = NumericProperty(30)
    acceptable_chars = u'ابپتثجچح آژئء خدذرزسشصضطظعغفقکگلمنوهی1234567890،abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[];,.":۱۲۳۴۵۶۷۸۹۰\n'
    str = StringProperty()

    def del_all(self):
        self.text = ""
        self.str = ""

    def insert_text(self, substring, from_undo=False):
        if not (substring in self.acceptable_chars):
            return
        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.str = self.str + substring
        self.text = Make_persian(self.str)
        substring = ""
        super(Text_f, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        self.str = self.str[0:len(self.str) - 1]
        self.text = Make_persian(self.str)


class Item(BoxLayout,FocusBehavior):
    def __init__(self, *args, **kwargs):
        super(Item, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finis_init, -1)

    def _finis_init(self, *dt):
        self.bind(focus=self.fb)
        self.radif = str(len(self.parent.children))

    def fb(self,*args):
        self.ids.stype.focus = True


class Num_text(TextInput):
    max_chars = NumericProperty(15)
    acceptable_chars = '1234567890،'
    sr = StringProperty()

    def del_all(self):
        self.text = ""
        self.sr = ""

    def insert_text(self, substring, from_undo=False):
        if not (substring in self.acceptable_chars):
            return
        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.sr = self.sr + substring
        self.text = Make_persian(self.sr)
        substring = ""
        super(Num_text, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        self.sr = self.sr[0:len(self.sr) - 1]
        self.text = Make_persian(self.sr)


class Sampler(BoxLayout):
    def __init__(self, *args, **kwargs):
        super(Sampler, self).__init__(*args, **kwargs)
        self.read_serial()
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, *dt):
        self.set_default_inputs()

    def read_serial(self):
        f = open("serial.txt", "r")
        sr = f.read()
        f.close()
        self.serial = int(sr)
        f = open("serial.txt", 'w')
        f.write(str(self.serial + 1))
        f.close()

    def extract_data(self):
        self.moshtari_dict = {
            "serial": self.ids.lb_serial.text,
            "date": self.ids.lb_date.text,
            "time": self.ids.lb_time.text,
            "mobile": self.ids.mobile.text,
            "hometel": self.ids.hometel.text,
            "name": self.ids.name.text,
            "address": self.ids.addr.text
        }
        self.services_data = list()
        for it in self.ids.services.children:
            lst = list()
            # with it.ids as it.ids:
            # lst.append(it.ids.radif.text)
            lst.append(it.ids.stype.text)
            lst.append(it.ids.model.text)
            lst.append(it.ids.serial.text)
            lst.append(it.ids.cost.text)
            lst.append([it.ids.peigiri_y.text, it.ids.peigiri_m.text, it.ids.peigiri_d.text])
            lst.append(it.ids.irad.text)
            lst.append(it.ids.description.text)
            self.services_data.append(lst)

        return self.moshtari_dict, self.services_data

    def set_default_inputs(self):
        date = JalaliDateTime.now()
        dates = "{year}/{month}/{day}".format(year=date.year, month=date.month, day=date.day)
        time = "{h}:{min}".format(h=JalaliDateTime.now().hour, min=JalaliDateTime.now().minute)
        self.ids.lb_serial.text = str(self.serial)
        self.ids.lb_date.text = dates
        self.ids.lb_time.text = time

    def add_item(self, *args):
        if len(self.ids.services.children) < 4:
            self.ids.services.add_widget(Item())

    def clear_all(self, *args):
        self.set_default_inputs()

        self.ids.mobile.del_all()
        self.ids.hometel.del_all()
        self.ids.name.del_all()
        self.ids.addr.del_all()

        self.ids.services.clear_widgets()
        self.add_item()

    # def enable_search(self):


class Search_box(BoxLayout):
    def __init__(self, caller, *args, **kwargs):
        self.caller = caller
        super(Search_box, self).__init__(*args, **kwargs)

    def do(self):
        self.extract_data()
        self.caller.done_search(self.se_serial, self.se_name, self.se_model, self.se_mobile)

    def extract_data(self):
        self.se_serial = self.ids.search_serial.text
        self.se_name = self.ids.search_name.text
        self.se_model = self.ids.search_model.text
        self.se_mobile = self.ids.search_mobile.text


class Searc_res(BoxLayout):
    def __init__(self, caller, *args, **kwargs):
        super(Searc_res, self).__init__(*args, **kwargs)
        self.caller = caller
        # self.ress = None

    def Put_data(self, ress):
        self.ress = ress

        self.ids.ress_name.text = ress[1]
        self.ids.ress_mobile.text = ress[2]
        self.ids.ress_hometel.text = ress[3]
        self.ids.ress_address.text = ress[4]
        self.ids.ress_num.text = ress[5]
        self.ids.ress_date.text = ress[6] + "-" + ress[7]

        self.ids.ress_stype.text = ress[8]
        self.ids.ress_model.text = ress[9]
        self.ids.ress_serial.text = ress[10]
        self.ids.ress_cost.text = ress[11]
        self.ids.ress_peigiri.text = ress[12]
        self.ids.ress_irad.text = ress[13]
        self.ids.ress_desceribtion.text = ress[14]

    def printer(self):
        # self.mosh = self.ress[1:8]
        self.moshtari_dict = {
            "serial": self.ress[5],
            "date": self.ress[6],
            "time": self.ress[7],
            "mobile": self.ress[2],
            "hometel": self.ress[3],
            "name": self.ress[1],
            "address": self.ress[4]
        }


        self.it =[self.ress[8:15]]
        # print(self.mosh)
        # print(self.it)
        pr.print_A5(self.moshtari_dict, self.it)
        pass


class mainscreen(BoxLayout):
    def __init__(self, *args, **kwargs):
        self.db = database.database_controller()
        super(mainscreen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, *args):
        self.search_box = Search_box(self)
        self.search_res_box = Searc_res(self)

        self.search_pop = Popup(title="Search", content=self.search_box, size_hint=(.5, .5),
                                pos_hint={'center_x': .5, "center_y": .5})
        self.search_res_pop = Popup(title="result", content=self.search_res_box, size_hint=(.6, .7),
                                    pos_hint={'center_x': .5, "center_y": .5})

    def do_save(self, *args):
        self.moshtari, self.items = self.ids.page.extract_data()
        self.db.add_item(moshtari=self.moshtari, items=self.items)

    def do_clearall(self, *args):
        self.ids.page.clear_all()

    def do_print(self, *args):
        self.moshtari, self.items = self.ids.page.extract_data()
        pr.print_A5(self.moshtari, self.items)
        pass

    def do_next(self, *args):
        self.ids.page.read_serial()
        self.do_clearall()

    def do_search(self):
        self.search_pop.open()

    def done_search(self, ser, name, model, mobile):
        ress = self.db.search(ser, name, model, mobile)

        if ress == "wrong":
            Popup(title="Error", content=Label(text="Not Found"), size_hint=(.4, .3),
                  pos_hint={"center_x": .5, "center_y": .5}).open()
        else:
            self.search_res_box.Put_data(ress)
            self.search_res_pop.open()
            self.search_pop.dismiss()


class reciptorApp(App):
    def build(self):
        return mainscreen()


reciptorApp().run()
