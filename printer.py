import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A5
from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
pdfmetrics.registerFont(TTFont('B Nazanin', 'B Nazanin.ttf'))
pdfmetrics.registerFont(TTFont('arial', 'arial.ttf'))
from bidi.algorithm import get_display
import arabic_reshaper


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


s_moshtari={
            "serial":  "1234",
            "date":    "1399/3/3",
            "time":    "9:41",
            "mobile":  "09125456323",
            "hometel": "02144243614",
            "name" :   Make_persian("جعفری"),
            "address": Make_persian("تتباایللسننممسننستتبننبمملککی")
        }
s_item = [["لبتاب","hp","1234","2800000","99-1-2","همه چی الا هر چی","ندرد"],
          ["لبتاب","hp","1234","2800000","99-1-2","همه چی الا هر چی","ندرد"]]


def send_to_printer():
    # import os
    #
    # os.startfile("Temp.pdf", "print")
    import win32api
    import win32print
    fname = "Temp.pdf"
    # win32api.ShellExecute(0, 'print', fname, None, '.', 0)
    win32api.ShellExecute(
        0,
        "printto",
        fname,
        '"%s"' % win32print.GetDefaultPrinter(),
        ".",
        0
    )


def print_A5(m,item):
    packet = io.BytesIO()
    # la5 =portrait(A5)
    can = canvas.Canvas(packet, pagesize=A5)
###########write every thing here###########################
    can.rotate(90)
    can.setFont("B Nazanin", 16)
    can.drawString(50,-15,m["serial"])
    can.drawString(50,-35,m["date"])
    can.drawString(50,-55,m["time"])
    can.drawString(30,-73,m["mobile"])
    can.drawString(200,-73,m["hometel"])
    can.drawRightString(500,-73,m["name"])
    can.setFont("B Nazanin", 14)
    can.drawRightString(500,-90,m["address"])

    can.setFont("B Nazanin", 12)
    for i in range(len(item)):
        print(item[i])
        can.rect(568,-142-(i*20),12,20)
        can.rect(512,-142-(i*20),56,20)
        can.rect(451,-142-(i*20),61,20)
        can.rect(408,-142-(i*20),43,20)
        can.rect(357,-142-(i*20),51,20)
        can.rect(313,-142-(i*20),44,20)
        can.rect(152,-142-(i*20),161,20)
        can.rect(15,-142-(i*20),137,20)
        can.drawCentredString(568+6,-135-(i*20),text=str(i+1))
        can.drawCentredString(512+(56/2),-135-(i*20),text=item[i][0])
        can.setFont("arial", 12)
        can.drawCentredString(451+(61/2),-135-(i*20),text=item[i][1])
        can.setFont("B Nazanin", 12)
        can.drawCentredString(408+(43/2),-135-(i*20),text=item[i][2])
        can.drawCentredString(357+(51/2),-135-(i*20),text=item[i][3])
        if len(item[i][4]) < 3:
            can.drawCentredString(313+(44/2),-135-(i*20),text=item[i][4][0]+"-"+item[i][4][1]+"-"+item[i][4][2])
        else:
            can.drawCentredString(313 + (44 / 2), -135 - (i * 20),
                                  text=item[i][4])
        can.setFont("arial", 12)
        can.drawCentredString(152+(161/2),-135-(i*20),text=item[i][5])
        can.drawCentredString(15+(137/2),-135-(i*20),text=item[i][6])
        can.setFont("B Nazanin", 12)
################################################################

    can.save()

    packet.seek(0)
    np = PdfFileReader(packet)
    nnp = np.getPage(0)
    # nnp.rotateClockwise(180)
    sample_pdf = PdfFileReader(open("mold.pdf","rb"))
    output = PdfFileWriter()
    page = sample_pdf.getPage(0)
    # page.rotateClockwise(270)

    page.mergePage(nnp)
    output.addPage(page)
    outputStream = open("Temp.pdf", "wb")
    output.write(outputStream)
    outputStream.close()

    send_to_printer()

# print_A5(s_moshtari,s_item)